<?php
require_once "functions.php";
session_start();
homeIfNoSession();
$conn = connectDB();
addCodeToMatchQueue($conn, $_SESSION["user"]->code);
updateQueueLastPing($conn, $_SESSION["user"]->code);
removeOldFromMatchQueue($conn, 5);


$length = getMatchQueueLength($conn);
$matchAvailable = getCodeMatchId($conn, $_SESSION["user"]->code) ? true : false;

if (!$matchAvailable && $length >= 2 && isItMatchEmissionTime($conn, MATCH_INTERVAL, MATCH_INTERVAL)) {
    makeNullMatch($conn, MATCH_INTERVAL);
}

if (!$matchAvailable && isItMatchEmissionTime($conn, MATCH_INTERVAL) && $length >= 2) {

    $users = getCodesInQueue($conn);
    makeMatches($conn, $users);
    $matchAvailable = true;
}

//$matchId = getCodeMatchId($conn, $_SESSION["user"]->code);

$conn->close();
echo '{"matchAvailable":"' . $matchAvailable . '", "queueLength":"' . $length . '"}';
