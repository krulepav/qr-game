<div class="w3-container">
    <?php
    if (isset($_POST["password"]) && isset($_POST["password-re"])) {
        $pwd = $_POST["password"];
        $pwdRe = $_POST["password-re"];

        if ($pwd != $pwdRe) {
            echo "<h3 class='w3-red'>CHYBA: Zadaná hesla nejsou stejná</h3> ";
        } else {
            register($conn, $code, $pwd);
            $_SESSION["user"] = getUser($conn, $code);
            header("Location: chat.php");
        }
    }
    ?>
    <script>
        function validate() {
            var form = document.forms["registration"];//["fname"].value;
            if (form["password"].value == null || form["password"].value == "") {
                alert("Je třeba vyplnit heslo");
                return false;
            }
            else if (form["password"].value != form ["password-re"].value) {
                alert("Hesla nejsou stejná");
                return false;
            }
            else if (form["agree"].checked == false) {
                alert("Doporučujeme přečíst důležité informace...");
                return false;
            }
        }
    </script>
    <h1>Registrace QR kódu do hry</h1>
    <form method="post" action="" onsubmit="return validate()" name="registration">
        <input type="password" placeholder="Heslo" class="w3-margin-bottom w3-input w3-border" name="password">
        <input type="password" placeholder="Heslo znovu" class="w3-margin-bottom w3-input w3-border" name="password-re">
        <label><input type="checkbox" class="w3-margin-bottom" name="agree">Přečetl jsem si a beru na vědomí <a
                href="index.php" target="_blank">důležité informace z úvodní stránky</a> (princip, bodování,
            ...)</label>
        <input type="submit" value="Registrovat" class="w3-input">
    </form>
</div>