<?php
require_once "functions.php";
session_start();
homeIfNoSession();
$conn = connectDB();
removeCodeFromMatchQueue($conn, $_SESSION["user"]->code);
$conn->close();