<a href="http://gegsusice.blogspot.cz/"><img src="geg-susice.png" alt="GEG Sušice"
                                             style="max-width: 250px; height: auto;"/></a>
<h1>QR Game</h1>
<div class="w3-left-align">
    <h2>Princip hry:</h2>
    Každý hráč má vytištěný svůj QR kód, který ho identifikuje. Na začátku kola (každých celých 5 minut) herní systém
    hráče pospáruje.
    Hráči navzájem neví, s kým jsou v páru, avšak můžou si se svým protějškem posílat textové zprávy. Úkolem každého
    hráče je najít v co nejkratší době "toho na druhé straně drátu".

    <h2>Budete potřebovat:</h2>
    <ul>
        <li>Chytré zařízení nejlépe typu telefon nebo tablet s foťákem, možností připojení na internet (wifi, mobilní
            data) a
            nainstalovanou čtečkou QR kódů (hledejte v příslušném obchodě Play / iTunes / Windows Store hesla "qr&nbsp;reader" nebo
            "qr&nbsp;scanner").
        </li>
        <li>Svůj unikátní QR kód, který získáte ve stánku GEGu</li>
    </ul>

    <h2>Jak začít:</h2>
    <ol type="1">
        <li>Vyzvedněte si na stánku svůj QR kód</li>
        <li>Naskenujte svůj kód a otevřete dekódovanou webovou stránku</li>
        <li>Zvolte si heslo (slouží k opětovnému přihlášení, pokud vás váš prohlížeč odhlásí)</li>
        <li>Počkejte ve frontě, dokud nezačne další kolo<br><!--<em>Poznámka: Pokud během čekání ve frontě zavřete okno
                prohlížeče, zhasnete display, případně zamknete telefon, systém vás z důvodu neaktivity automaticky
                dočasně z fronty vyřadí. Pro opětovné zařazení do fronty stačí např. tapnout kamkoliv na webové
                stránce.</em>--></li>
        <li>Jakmile začne kolo a systém vám vybere parťáka, můžete si s ním začít psát a snažit se najít jeden druhého
        </li>
        <li>Až se se svým protějškem najdete, můžete zavřít okno prohlížeče, naskenovat QR kód svého spoluhráče a
            otevřít dekódovanou webovou stránku. Systém vám přidělí body a můžete opět do fronty na další kolo.
        </li>
    </ol>

    <h2>Bodování:</h2>
    <ul>
        <li>Na začátku kola máte 420 bodů</li>
        <li>Každou vteřinu od začátku kola se odečte 1 bod</li>
        <li>Za každý odeslaný znak se <em>oběma</em> hráčům odeberou 2 body</li>
        <li>Po naskenování spoluhráčova QR kódu systém navýší vaše celkové skóre o odpovídající počet bodů</li>
        <li>Pokud váš bodový zisk v daném kole je menší nebo roven 0, kolo pro vás končí, celkové skóre se nemění</li>
    </ul>
</div>
<hr>
<footer class="w3-small">Hra je ve zkušebním režimu. Je tedy možné, že během hraní objevíte nějakou chybu. Budeme rádi,
    pokud nám tuto chybu nahlásíte, abychom ji mohli v dalších verzích opravit. Omlouváme se za možné problémy a
    děkujeme za pochopení.<br>Kontakt: pavel@krulec.tk
</footer>