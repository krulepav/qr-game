<?php
session_start();
require_once "functions.php";
homeIfNoSession();
$conn = connectDB();
//setUserMatchIdAndOpponent($conn, $_SESSION["user"]);

if (is_null($_SESSION["user"]->matchId)) {
    header("Location: matchQueue.php");
}

$match = getMatch($conn, $_SESSION["user"]->matchId);
$profitBase = (7 * 60 - (time() - strtotime($match->timestamp)));
$profitPenalisation = getPenalisation($conn, $_SESSION["user"]->matchId);
if (($profitBase - $profitPenalisation) <= 0) {
    setMatchFoundAtNow($conn, $match->id);
    header("Location: matchQueue.php");
}
$conn->close();
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>
        QR Game
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="w3.css">
    <style>
        @font-face {
            font-family: "Lucida Console";
            src: url("LUCON.TTF");
        }
    </style>
    <script>
        var lastMessageId = 0;
        var profitBase = <?php echo $profitBase;?>;
        var profitPenalty = 0;
        var matchTimestamp = new Date(<?php echo strtotime($match->timestamp * 1000); ?>);//new Date("<?php echo date('Y-m-d G:i:s', strtotime($match->timestamp)); ?>");
        var timeDiff = new Date(<?php echo (time()*1000); ?>) - new Date();
        const COST_PER_LETTER = 2;
        var autoUpdate = true;
        var updateProfitIntervalID;

        function init() {
            //$("updateNow").addEventListener("click", update);
            $("messageForm").addEventListener("submit", sendMessage);
            $("message").addEventListener("input", updateCost);
            /*$("autoUpdate").addEventListener("change", function () {
             autoUpdate = $("autoUpdate").checked;
             console.log("Autoupdate: " + autoUpdate);
             });
             $("logout").addEventListener("click", function () {
             location.href = "logout.php";
             });*/
            updateProfitIntervalID = setInterval(updateProfit, 1000);
            setInterval(autoUpdateFnc, 2000);
            update();
        }

        function $(name) {
            return document.getElementById(name);
        }

        function update() {
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (xhttp.readyState == 4 && xhttp.status == 200) {
                    console.log("Update: " + xhttp.responseText);
                    if (xhttp.responseText != "") {
                        var resp = JSON.parse(xhttp.responseText);
                        for (var i = 0; i < resp.length; i++) {
                            if (lastMessageId != resp[i].id) {
                                lastMessageId = resp[i].id;
                                insertMsg(resp[i].sender + resp[i].message);
                                decreaseProfit(resp[i].cost);
                            }
                        }
                    }
                    else {
                        return false;
                    }
                }
            };
            xhttp.open("POST", "getMessage.php", true);
            xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhttp.send("lastMessageId=" + lastMessageId);
        }

        function insertMsg(message) {
            $("massageFeed").value = message + "\n" + $("massageFeed").value;
        }

        function sendMessage(event) {
            event.preventDefault();

            var len = $("message").value.length;
            if (len <= 0)
                return;
            else {
                sendMessageAsync();
                $("message").value = "";
                updateCost();
            }
        }

        function sendMessageAsync() {
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (xhttp.readyState == 4 && xhttp.status == 200) {
                    console.log("Send: " + xhttp.responseText);
                    update();
                    if (xhttp.responseText != "") {
                        alert(xhttp.responseText);
                        return true;
                    }
                    else {
                        return true;
                    }
                }
            };
            xhttp.open("POST", "sendMessage.php", true);
            xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhttp.send("message=" + $("message").value);
        }

        function updateCost() {
            var len = $("message").value.length;
            $("cost").innerHTML = "Cena: " + len * COST_PER_LETTER;
        }

        function decreaseProfit(amount) {
            profitPenalty += Number(amount);
            if (profit < 0) {
                profit = 0;
            }
            updateProfit();
        }

        function updateProfit() {
            var profit = getFinalProfit();
            if (profit <= 0) {
                profit = 0;
                alert("Čas vypršel");
                location.href = "chat.php";
                clearInterval(updateProfitIntervalID);
                //location.href = "matchQueue.php";
            }
            $("profit").innerHTML = "Zisk: " + profit;
        }

        function getFinalProfit() {
            return profitBase - profitPenalty - Math.round((new Date() - matchTimestamp + timeDiff) / 1000);
        }

        function autoUpdateFnc() {
            if (autoUpdate) {
                update();
            }
        }
    </script>
</head>
<body class="w3-content" onload="init()">
<?php
/*var_dump($match);
echo "<br>";
var_dump($_SESSION["user"]);
echo "<br>";*/
?>
<form id="messageForm">
    <div class="w3-half">
        <textarea style="width: 100%;  font-family: 'Lucida Console'" disabled class="w3-disabled" rows=5 id="massageFeed"></textarea>
    </div>
    <div class="w3-container w3-half">
        <div class="w3-center"><?php echo "Jsi " . generateName($_SESSION["user"]); ?></div>
        <!--<input type="button" value="Aktualizovat nyní" class="w3-btn w3-light-grey" id="updateNow"><label
            class="w3-right"><input
                type="checkbox" class="w3-check" id="autoUpdate" checked> Auto aktualizace</label><br>-->
        <span id="cost">Cena: 0</span><span id="profit" class="w3-right"></span>
        <input type="text" placeholder="Zpráva" id="message" class="w3-margin-bottom w3-input w3-border"
               autocomplete="off">
        <input type="submit" value="Odeslat" class="w3-margin-bottom w3-input">
        <!--<input type="button" value="Odhlásit" class="w3-margin-bottom w3-input" id="logout">-->
    </div>
</form>
</body>
</html>