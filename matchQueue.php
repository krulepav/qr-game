<?php
require "functions.php";
session_start();
homeIfNoSession();
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>
        QR Game
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="w3.css">

    <script>
        var timeDiff = new Date(<?php echo(time() * 1000); ?>) - new Date();
        function init() {
            //$("leave").addEventListener("click", leaveQueue);
            setInterval(ping, 1000);
            setInterval(updateNextRoundAt, 1000);
            ping();
            updateNextRoundAt();
        }

        function $(name) {
            return document.getElementById(name);
        }

        function leaveQueue() {
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (xhttp.readyState == 4 && xhttp.status == 200) {
                    location.href = "index.php";
                }
            };
            xhttp.open("POST", "leaveQueue.php", true);
            xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhttp.send();
            return false;
        }

        function updateNextRoundAt() {
            var date = new Date(Math.ceil((Math.round(new Date() / 1000)) / (2 * 60)) * (2 * 60) * 1000);
            var countdown = new Date(date - new Date() + timeDiff);
            $("nextRoundAt").innerHTML = addZero(countdown.getMinutes()) + ":" + addZero(countdown.getSeconds()) + " (ve " + date.getHours() + "." + addZero(date.getMinutes()) + ")";
        }

        function ping() {
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (xhttp.readyState == 4 && xhttp.status == 200) {
                    console.log(xhttp.responseText);
                    var resp = JSON.parse(xhttp.responseText);
                    $("queueLenght").innerHTML = resp.queueLength;
                    if (resp.matchAvailable) {
                        //$("queueLenght").innerHTML += "<br><input type='button' value='Chat'>";
                        location.href = "chat.php";
                    }
                }
            };
            xhttp.open("POST", "pong.php", true);
            xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhttp.send();
            return false;
        }

        function addZero(i) {
            if (i < 10) {
                i = "0" + i;
            }
            return i;
        }
    </script>
</head>
<body class="w3-content" onload="init()">
<div class='w3-container'>
    <h1 class="w3-green w3-padding">Jsi ve frontě!</h1>
    <h3>Další kolo začne za <span id="nextRoundAt"></span></h3>

    <h3>Počet hráčů ve frontě: <span id="queueLenght">1</span></h3>
    <!--<input type="button" class="w3-btn" id="leave" value="Opustit frontu">-->
    <div>
        Tvé skóre: <?php echo $_SESSION["user"]->score; ?><br>
    </div>

    <?php
    $conn = connectDB();
    $highscores = getTopTen($conn);
    $conn->close();
    if ($highscores) {
        echo "<div>";
        echo "<h4>TOP ".count($highscores)." skore:</h4>";
        for ($i = 0; $i < count($highscores); $i++) {
            echo ($i + 1) . ". " . $highscores[$i][0] . "<br>";
        }
        echo "</div>";
    }
    ?>

    <hr>
    <div class="w3-small">
        <em>Poznámka: Pokud během čekání zavřete toto okno, zhasnete display, případně zamknete telefon, systém vás z
            důvodu neaktivity automaticky dočasně z fronty vyřadí. Pro opětovné zařazení do fronty stačí např. kamkoliv
            tapnout.</em>
    </div>
</div>
</body>
</html>