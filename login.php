<div class="w3-container">
    <?php
    if (isset($_POST["password"])) {
        $pwd = $_POST["password"];

        $login = login($conn, $code, $pwd);
        if (!$login) {
            echo "<h3 class='w3-red'>CHYBA: Špatné heslo</h3> ";
        } else {
            $_SESSION["user"] = getUser($conn, $code);
            header("Location: chat.php");
        }
    }
    ?>
    <h1>Opětovné přihlášení do hry</h1>
    <form method="post" action="">
        <input type="password" placeholder="Heslo" class="w3-margin-bottom w3-input w3-border" name="password">
        <input type="submit" value="Přihlásit" class="w3-input">
    </form>
</div>