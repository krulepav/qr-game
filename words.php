<?php
function wordsGetNames()
{
    return ["jednorožec", "nosorožec", "kedlubna", "čupakabra", "čumbrlajn", "kakáb", "babička Míla", "rohožka", "brouk", "dobrá víla", "hej počkej", "prezident", "sluníčko", "rampouch", "kámen",
        "rašple", "virůsek", "traktorista"];
}

function wordsGetBannedWords()
{
    return ["most", "strom", "podium", "kios", "kolotoč", "kolotoc"];
}