<?php
function printForm()
{
    echo "    <h1>Přihláška na closed beta test</h1>
    <form method=\"post\" action=\"\">
        <input type=\"text\" placeholder=\"Jméno\" class=\"w3-margin-bottom w3-input w3-border\" name=\"firstname\" required>
        <input type=\"text\" placeholder=\"Příjmení\" class=\"w3-margin-bottom w3-input w3-border\" name=\"lastname\" required>
        <input type=\"email\" placeholder=\"E-mail\" class=\"w3-margin-bottom w3-input w3-border\" name=\"email\" required>
        <div class=\"w3-center\">Kód s hudoménsky krutopřísně tajným heslem</div>
        <img src=\"heslo.png\" alt=\"QR kód s heslem\">
        <input type=\"text\" placeholder=\"Hudoménsky krutopřísně tajné heslo\" class=\"w3-margin-bottom w3-input w3-border\" name=\"password\" required>
        <input type=\"submit\" value=\"Přihlásit se\" class=\"w3-input w3-blue w3-margin-bottom\">
    </form>";
}

?>
<div class="w3-left-align">
    <h2>Pricip hry:</h2>
    Každý hráč ve skupině má vytištěný QR kód, který ho identifikuje. Na začátku kola herní systém hráče pospáruje.
    Hráči navzájem neví, s kým jsou v páru, avšak můžou si se svým protějškem posílat textové zprávy. Úkolem každého
    hráče je najít v co nejkratší době "toho na druhé straně drátu".

    <h2>Budete potřebovat:</h2>
    <ul>
        <li>Chytré zařízení nejlépe typu telefon nebo tablet s foťákem, možností připojení na internet (wifi, mobilní
            data) a
            nainstalovanou čtečkou QR kódů (hledejte v příslušném obchodě Play / iTunes / Windows Store hesla "qr
            reader" nebo
            "qr scanner").
        </li>
        <li>Vytištěný QR kód, který dostanete e-mailem.</li>
    </ul>

    <h2>Časoprostor:</h2>
    <ul>
        <li>Sobota 2. 4., 15:00, ostrov Santos</li>
        <li>Testování by nemělo trvat déle než 1,5 hodiny</li>
    </ul>

    <h2>Kontakt:</h2>
    Pavel Krulec, krulec.pavel@gmail.com, 737 589 988
</div>
<div class="w3-container">
    <?php
    if (isset($_POST["firstname"]) && isset($_POST["lastname"]) && isset($_POST["email"]) && isset($_POST["password"])) {
        $firstName = $_POST["firstname"];
        $lastName = $_POST["lastname"];
        $email = $_POST["email"];
        $password = $_POST["password"];

        if ($password == "beruška") {
            $conn = connectDB();
            $sql = $conn->query("INSERT INTO beta VALUES (null, '$firstName', '$lastName', '$email')");
            echo $conn->error;
            if ($sql) {
                echo "<h1 class='w3-green'>Tvá žádost byla přijata, díky za zájem. Budu tě kontaktovat</h1>";
            } else
                echo "<h1 class='w3-red'>Ups, něco se pokazilo, Pavel to blbě napsal. Dej mu vědět.</h1>";
        } else {
            echo "<h1 class='w3-red'>Jaj, špatné heslo. Zkus naskenovat ten QR kód ;)</h1>";
            printForm();
        }
    } else {
        printForm();
    }
    ?>
</div>
