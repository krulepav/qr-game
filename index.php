<?php
require_once "functions.php";
session_start();
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>
        QR Game
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="w3.css">
    <link href="favicon.png" rel="icon" type="image/png"/>
</head>
<body class="w3-content w3-margin-bottom w3-margin-top">
<?php
if (!isset($_GET["qr"])) {
    if (isset($_SESSION["user"])) {
        header("Location: chat.php");
    } else {
        echo "<div class='w3-container' style='text-align: center;'>";
        require_once "homepage.php";
        echo "</div>";
    }
} else {
    $start = mktime(14, 0, 0, 5, 14, 2016);
    $end = mktime(15, 0, 0, 5, 14, 2016);
    if (time() < $start) {
        echo "<div class='w3-container' style='text-align: center;'>";
        require_once "homepage.php";
        echo "</div>";
        echo "<script>alert('Hra začne v " . date("G.i", $start) . "');</script>";
    } else if (time() > $end) {
        echo "<div class='w3-container' style='text-align: center;'>";
        echo '<a href="http://gegsusice.blogspot.cz/"><img src="geg-susice.png" alt="GEG Sušice"
                                             style="max-width: 250px; height: auto;"/></a>';
        echo "<h1>Hra již skončila</h1>";
        echo "<h2>Děkujeme za účast</h2>";
        if (isset($_SESSION["user"]))
            echo "Tvé skóre: " . $_SESSION["user"]->score . "<br>";
        $conn = connectDB();
        $highscores = getTopTen($conn);
        $conn->close();
        if ($highscores) {
            echo "<div>";
            echo "<h4>TOP " . count($highscores) . " skore:</h4>";
            for ($i = 0; $i < count($highscores); $i++) {
                echo ($i + 1) . ". " . $highscores[$i][0] . "<br>";
            }
            echo "</div>";
        }

        echo "</div>";
    } else {
        $code = $_GET["qr"];
        $conn = connectDB();
        if ($password = checkCode($conn, $code)) {
            if ($password == "null") {
                require "register.php";
            } else {
                if (isset($_SESSION["user"])) {
                    if ($_SESSION["user"]->code == $code) {
                        header("Location: chat.php");
                    } else {
                        require "qr-found.php";
                    }
                } else {
                    require "login.php";
                }
            }
        } else {
            echo "<div class='w3-container'><h1>CHYBA: Kód nenalezen.</h1><h3>Pokud problém přetrvává, kontaktujte organizátora.</h3></div>";
        }
        $conn->close();
    }
}
?>
</body>
</html>