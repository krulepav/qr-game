<?php
define("MATCH_INTERVAL", 60 * 2);

function homeIfNoSession()
{
    if (!isset($_SESSION["user"])) {
        header("Location: index.php");
        return;
    } else {
        $conn = connectDB();
        $_SESSION["user"] = getUser($conn, $_SESSION["user"]->code); //update user
        $conn->close();
    }
}

function connectDB()
{
    $DBservername = "";
    $DBusername = "";
    $DBpassword = "";
    $DBdbname = "";
    require "db-credentials.php";

    $conn = new mysqli($DBservername, $DBusername, $DBpassword, $DBdbname);

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    $conn->query("SET NAMES 'utf8'");
    return $conn;
}

function sendMessage($conn, $sender, $matchId, $message)
{
    $message = $conn->real_escape_string($message);
    $sql = $conn->query("INSERT INTO messages (sender, matchId, message) VALUES ('$sender', $matchId, '$message')");
    if ($sql) {
        return $conn->insert_id;
    } else {
        return false;
    }
}

function getMessages($conn, $matchId, $lastMessageId, $myCode)
{
    $json = '[';
    $sql = $conn->query("SELECT * FROM messages WHERE matchId = $matchId AND id > $lastMessageId");
    if ($sql && $sql->num_rows > 0) {
        while ($row = $sql->fetch_assoc()) {
            $senderUser = getUser($conn, $row["sender"]);//;($row["sender"] == $myCode) ? "<já>\\t\\t\\t" : "<ten druhý>\\t";
            $sender = str_pad(generateName($senderUser), maxNameLength());
            $sender .= ($row["sender"] == $myCode) ? "> " : "< ";
            $json .= '{"id": ' . $row["id"] . ', "sender": "' . $sender . '", "cost":"' . strlen($row["message"]) * 2 . '", "message":"' . str_replace('"', '\"', $row["message"]) . '"}, ';
        }
        $json = substr($json, 0, -2);
    } else
        return false;
    $json .= ']';
    return $json;
}

function checkCode($conn, $code)
{
    $sql = $conn->query("SELECT password FROM qrcodes WHERE code = '$code'");
    if ($sql->num_rows > 0) {
        $password = $sql->fetch_assoc()["password"];
        if (is_null($password)) {
            return "null";
        } else {
            return $password;
        }
    } else {
        return false;
    }
}

function register($conn, $code, $password)
{
    $password = $conn->real_escape_string($password);
    $sql = $conn->query("UPDATE qrcodes SET password = '$password' WHERE code = '$code'");
}

function login($conn, $code, $password)
{
    $sql = $conn->query("SELECT * FROM qrcodes WHERE code = '$code'");
    if ($sql->num_rows > 0) {
        $pwd = $sql->fetch_assoc()["password"];
        return ($pwd == $password);
    } else {
        return false;
    }
}

function getUser($conn, $code)
{
    $sql = $conn->query("SELECT * FROM qrcodes WHERE code = '$code'");
    if ($sql->num_rows > 0) {
        $row = $sql->fetch_assoc();
        $user = new stdClass();
        $user->code = $row["code"];
        $user->password = $row["password"];
        $user->score = $row["score"];
        $user->matchId = null;
        $user->opponent = null;
        setUserMatchIdAndOpponent($conn, $user);
        return $user;
    } else {
        return false;
    }
}

function setUserMatchIdAndOpponent($conn, $user)
{
    $sql = $conn->query("SELECT * FROM matches WHERE (player1 = '$user->code' OR player2 = '$user->code') AND foundAt IS NULL");
    if ($sql->num_rows > 0) {
        $row = $sql->fetch_assoc();
        $user->matchId = $row["id"];
        if ($row["player1"] == $user->code) {
            $user->opponent = $row["player2"];
        } else {
            $user->opponent = $row["player1"];
        }
    } else {
        $user->matchId = null;
        $user->opponent = null;
    }
    return $user;
}

function getMatch($conn, $matchId)
{
    $sql = $conn->query("SELECT * FROM matches WHERE id = $matchId");
    if ($sql->num_rows > 0) {
        $row = $sql->fetch_assoc();
        $match = new stdClass();
        $match->id = $row["id"];
        $match->player1 = $row["player1"];
        $match->player2 = $row["player2"];
        $match->timestamp = $row["timestamp"];
        $match->foundAt = $row["foundAt"];
        return $match;
    }
    return false;
}

function getMessagesLenght($conn, $matchId)
{
    $sql = $conn->query("SELECT message FROM messages WHERE matchId = $matchId");
    if ($sql && $sql->num_rows > 0) {
        $messagesLength = 0;
        while ($row = $sql->fetch_assoc()) {
            $messagesLength += strlen($row["message"]);
        }
        return $messagesLength;
    } else {
        return false;
    }
}

function getPenalisation($conn, $matchId)
{
    return getMessagesLenght($conn, $matchId) * 2;
}

function getMatchQueueLength($conn)
{
    $sql = $conn->query("SELECT code FROM matchqueue");
    if ($sql) {
        return $sql->num_rows;
    } else {
        return false;
    }
}

function isCodeInMatchQueue($conn, $code)
{
    $sql = $conn->query("SELECT code FROM matchqueue WHERE code = '$code'");
    if ($sql && $sql->num_rows > 0) {
        return true;
    } else {
        return false;
    }
}

function addCodeToMatchQueue($conn, $code)
{
    if (isCodeInMatchQueue($conn, $code))
        return false;
    $sql = $conn->query("INSERT INTO matchqueue (code) VALUES ('$code')");
    if ($sql) {
        return true;
    } else {
        return false;
    }
}

function removeCodeFromMatchQueue($conn, $code)
{
    $sql = $conn->query("DELETE FROM matchqueue WHERE code ='$code'");
    if ($sql) {
        return true;
    } else {
        return false;
    }
}

function updateQueueLastPing($conn, $code)
{
    $sql = $conn->query("UPDATE matchqueue SET lastUpdate = '" . date('Y-m-d G:i:s') . "' WHERE code = '$code'");
}

function removeOldFromMatchQueue($conn, $age)
{
    $sql = $conn->query("DELETE FROM matchqueue WHERE lastUpdate < '" . date('Y-m-d G:i:s', time() - $age) . "'");
}

function setUserScore($conn, $code, $score)
{
    $sql = $conn->query("UPDATE qrcodes SET score = $score WHERE code = '$code'");
}

function setMatchFoundAtNow($conn, $matchId)
{
    $sql = $conn->query("UPDATE matches SET foundAt = '" . date('Y-m-d G:i:s') . "' WHERE id = '$matchId' AND foundAt IS NULL");
}

function getCodeMatchId($conn, $code)
{
    $sql = $conn->query("SELECT id FROM matches WHERE (player1 = '$code' OR player2 = '$code') AND foundAt IS NULL");
    if ($sql && $sql->num_rows > 0) {
        return $sql->fetch_assoc()["id"];
    } else {
        return false;
    }
}

function isItMatchEmissionTime($conn, $maxSecondsAge, $timeOffset = 0)
{
    $sql = $conn->query("SELECT timestamp FROM matches WHERE timestamp >= '" . date('Y-m-d G:i:s', floor((time() - $timeOffset) / $maxSecondsAge) * $maxSecondsAge) . "'");
    if ($sql && $sql->num_rows > 0) {
        return false;//$sql->fetch_assoc()["timestamp"]; //
    } else {
        return true;
    }
}

function getCodesInQueue($conn)
{
    $sql = $conn->query("SELECT code FROM matchqueue");
    $users = array();
    while ($row = $sql->fetch_assoc()) {
        array_push($users, $row["code"]);
    }
    return $users;
}

function makeMatches($conn, $users)
{
    shuffle($users);
    $player1 = array_pop($users);
    $player2 = array_pop($users);
    while ($player1 != NULL && $player2 != NULL) {
        $date = date('Y-m-d G:i:s', floor(time() / MATCH_INTERVAL) * MATCH_INTERVAL);
        $sql = $conn->query("INSERT INTO matches (player1, player2, timestamp)
              VALUES ('$player1', '$player2', '" . $date . "')");

        $sql = $conn->query("DELETE FROM matchQueue WHERE code = '$player1' OR code = '$player2'");

        $player1 = array_pop($users);
        $player2 = array_pop($users);
    }
}

function makeNullMatch($conn, $roundInterval)
{
    $date = date('Y-m-d G:i:s', (floor(time() / $roundInterval) * $roundInterval));//$date = date('Y-m-d G:i:s', (floor(time() / $roundInterval) * $roundInterval) + round($roundInterval * .5));
    $sql = $conn->query("INSERT INTO matches (player1, player2, timestamp, foundAt)
              VALUES (NULL, NULL, '$date',  '$date')");
}

function generateName($user)
{
    require_once "words.php";
    $names = wordsGetNames();
    $name = $names[(abs(crc32($user->code)) + $user->score + $user->matchId) % count($names)];
    return $name;
}

function containsBannedWords($message)
{
    require_once "words.php";
    $bannedWords = wordsGetBannedWords();
    return contains($message, $bannedWords, false);
}

function contains($string, $array)
{
    $words = "";
    foreach ($array as $token) {
        if (stristr($string, $token) !== FALSE) {
            $words .= $token . "\n";
        }
    }
    return strlen($words) > 0 ? $words : false;
}


function maxNameLength()
{
    require_once "words.php";
    $length = 0;
    $names = wordsGetNames();
    foreach ($names as $token) {
        if (strlen($token) > $length)
            $length = strlen($token);
    }
    return $length;
}


function getTopTen($conn)
{
    $sql = $conn->query("SELECT score FROM qrcodes WHERE score > 0 ORDER BY score DESC LIMIT 5");
    if ($sql && $sql->num_rows > 0) {
        return $sql->fetch_all();
    } else {
        return false;
    }
}