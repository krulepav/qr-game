<?php
session_start();
$sender = $_SESSION["user"]->code;
$matchId = $_SESSION["user"]->matchId;
$message = $_POST["message"];

/*$sender = $_POST["sender"];
$matchId = $_POST["matchId"];
$message = $_POST["message"];*/

require_once "functions.php";
$bannedWords = containsBannedWords($message);
if ($bannedWords) {
    echo "Zpráva nebyla odeslána, protože obsahovala následující nepovolená slova:\n".$bannedWords;
} else {
    $conn = connectDB();
    $lastId = sendMessage($conn, $sender, $matchId, $message);
    $conn->close();
}