<?php
if (!isset($_POST["lastMessageId"])) {
    header("Location: index.php");
    return;
}
session_start();
$matchId = $_SESSION["user"]->matchId;
//$matchId = $_POST["matchId"];
$lastMessageId = $_POST["lastMessageId"];

require_once "functions.php";
$conn = connectDB();

$json = getMessages($conn, $matchId, $lastMessageId, $_SESSION["user"]->code);

$conn->close();
if ($json)
    echo $json;
else
    echo "";