<?php
$user = $_SESSION["user"];
if ($code == $user->opponent) {
    $length = getMessagesLenght($conn, $user->matchId);
    $match = getMatch($conn, $user->matchId);
    $profitBase = (7 * 60 - (time() - strtotime($match->timestamp)));
    $profitPenalisation = getPenalisation($conn, $user->matchId);
    $score = $profitBase - $profitPenalisation;
    if ($score <= 0) {
        $score = 0;
    }
    $user->score += $score;
    setMatchFoundAtNow($conn, $user->matchId);
    setUserScore($conn, $user->code, $user->score);
    $user = setUserMatchIdAndOpponent($conn, $user);
    echo "<div class='w3-green w3-container'><h1>Načetl jsi správný kód. Tvůj zisk: $score</h1>
    <h3>Celkové skóre: " . $user->score . "</h3></div><input type='button' class='w3-input' value='OK' onclick='location.href=\"matchQueue.php\"'>";
} else {
    echo "<div class='w3-red w3-container'><h1>Načetl jsi špatný kód.</h1></div><input type='button' class='w3-input' value='OK' onclick='location.href=\"matchQueue.php\"'>";
}